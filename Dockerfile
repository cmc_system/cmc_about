FROM ruby:3 AS build

WORKDIR /app

RUN gem install jekyll bundler
RUN apt-get update -y && apt-get install -y vim git g++ gcc tmux pip
RUN pip install codespell

COPY Gemfile Gemfile.lock ./
RUN bundle install

COPY . ./

RUN bundle exec jekyll build -d public

FROM nginx

COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build /app/public/ /usr/share/nginx/html/
