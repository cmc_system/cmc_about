---
layout: default
title: Calendar
---

The calendar allow you to create events. These events are visible to
all your employees.

Features:
* You can choose if an event is for a time interval or for the entire day.
* You can add a description to every event.
* The calendar can be displayed in different ways: Per month, per week or
  in the per day mode. Can be integrated with google calendar.

<img src="/assets/images/calendar.png" style="width:100%;" alt="Calendar">
