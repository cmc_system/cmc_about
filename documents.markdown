---
layout: default
title: Documents
---

Your employees can upload and organize the documents in a structured way.
This removes the pain to have to send the documents to every employee. It is
also much faster to find something there than have to lock for the physical document.

<img loading="lazy" src="/assets/images/archive_index.png" style="width:100%;" alt="Documents">

## Upload big files

Sometimes it is useful to share big files in the company. Any kind of file is
allowed. For now, there are no restrictions on quantity or size.

<img src="/assets/images/Screenshot-from-2020-06-23-14-46-18.png" style="width:100%;" alt="Upload big files">
