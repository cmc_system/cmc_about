---
layout: default
title: Manage projects
---

Ticktensio allows you to manage multiple projects.

## Organize projects by their status

When you have multiple projects, then you will have projects that are finished,
in progress … Ticktensio allows you to organize the Projects by these
 statuses.

<img loading="lazy" src="/assets/images/Screenshot-from-2020-06-22-19-45-42.png" style="width:100%;" alt="Organize projects">

## Organize projects inside a board

Projects can be organized in a drag and drop board. Just create a new project
status and chose the board option.

<img loading="lazy" src="/assets/images/Screenshot-from-2020-06-22-20-11-32.png" style="width:100%;" alt="Organize projects inside a board">

## Reminders

You can add reminders to projects. A reminder is a Ticket that is assigned to
a project and has a “due” or “deadline” field

<img loading="lazy" src="/assets/images/Screenshot-from-2020-06-22-20-19-29.png" style="width:100%;" alt="Reminders">
