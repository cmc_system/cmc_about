---
layout: default
title: Self hosting
---

You can install the platform on your own server. This allows to maintain all
 your data 100% private. This does not mean that we do not treat your data
 with the mayor discretion possible.

Also, an advantage of self-hosting the application is that it allows you to
 extend it by yourself. All the public code is licensed with MIT license.
 This means that you can do with the code what you want.

If you are considering to self-host it only because you need a small
feature and plan to extend this application for this, then maybe it would
be a good idea to first ask us on our support page if we are willing to
implement it.

The installation instructions can be found on the
[gitlab](https://gitlab.com/cmc_system/cmc) page of this project.
