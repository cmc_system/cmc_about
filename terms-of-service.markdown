---
layout: default
title: Terms of service
---

The following terms and conditions govern all use of the Ticktensio.com website, or any other website owned and operated by “Aron Wolf”
which incorporates these terms and conditions (the “Website”), including all content, services and support packages provided on via the
Website. The Website is offered subject to your acceptance without modification of all of the terms and conditions contained herein and all
other operating rules, policies (including, without limitation, procedures that may be published from time to time on this Website by “Aron Wolf”
(collectively, the “Agreement”).

Please read this Agreement carefully before accessing or using the Website. By accessing or using any part of the Website, you agree to be
bound by the terms and conditions of this Agreement. If you do not agree to all the terms and conditions of this Agreement, then you may
not access the Website or use any of the services. If these terms and conditions are considered an offer by “Aron Wolf”, acceptance is
expressly limited to these terms. The Website is available only to individuals who are at least 13 years old.

**Your Account and Website**. If you create an account and/or organization on the Website, you are responsible for maintaining the security
of your account and/or organization, and you are fully responsible for all activities that occur under the account and/or organization and
any other actions taken in connection with the account and/or organization. You must immediately notify “Aron Wolf” of any unauthorized use
of your account (or organization) or any other breaches of security. “Aron Wolf” will not be liable for any acts or omissions by You, including
any damages of any kind incurred as a result of such acts or omissions.

**Acceptable Use of Your Account and the Website**. By accepting this Agreement, you agree not to use, encourage, promote, or facilitate others
to use, the Website or your account (or organization) in a way that is harmful to others (“Acceptable Use”). Examples of harmful use include,
but are not limited to, engaging in illegal or fraudulent activities, infringing upon others’ intellectual property rights, distributing harmful content…

**Availability**. Services and/or Website may be unavailable for limited time because of maintenance or technical problems.

**Changes to Ticktensio Services; Amendments**. We are allowed to change, suspend, or discontinue the Ticktensio.com Services, or any part of them, at any time
without notice. We may amend any of this Agreement’s terms at our sole discretion by posting the revised terms on the ticktensio.com website. Your continued
use of Ticktensio.com Services after the effective date of the revised Agreement constitutes your acceptance of the terms. We will try to be polite and maintain
you informed about important changes, but we are not forced to do it.

**Termination**.  We may terminate your access to all or any part of the Website or/and services at any time, with or without cause, with or without notice,
effective immediately.

**Privacy Policy**. [See here](/privacy-policy).
