---
layout: default
title: Ticket board
---

The Ticket Board allows tickets/issues to be moved from one status to another.
It is similar to other kanban boards like Trello or that one that is
present on Jira.

## Organize tickets per status

Tickets are organized by columns. Every column represents a status. The tickets
can be moved from one to other by trag and drop. On the right corner of every
column exist a plus button that allow to create new tickets with the corresponding
status.

<img loading="lazy" src="/assets/images/Screenshot-from-2020-12-12-17-34-30.png" style="width:100%;" alt="Ticket board">

## Board per project

It is possible to switch between the boards of different projects by a select
box on the right-top corner. The status of the select box is saved automatically
so that when the user visit again the board view, the same project-board appears
that the last time he visited the page.

<img loading="lazy" src="/assets/images/board_projects.png" style="width:100%;" alt="Ticket board per project>
