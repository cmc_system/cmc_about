---
layout: default
title: Wiki
---

Allow to organize the information on your organization.

## Organized your data

The information can be structured in a tree way as you can see in the
screenshot. This makes it much easier to find what you are searching for.
There are no limits to how deep this tree can become.

<img loading="lazy" src="/assets/images/wiki_index.png" style="width:100%;" alt="Wiki">

## Create complex content

Every page can be created/edited in a rich way using Markdown. Markdown is an
easy and powerful language that is used in thousands of projects. His popularity
comes from that it allows creating complex content without having to overuse the
user interface. The editor also offers a more traditional way (WYSWYG) similar to
what you are accustomed to your Office program.

<img loading="lazy" src="/assets/images/wiki_page-1.png" style="width:100%;" alt="Wiki page">
